#!/usr/bin/env python3
# RVGL Launcher (Generic)
# Generate json for app info and updates.

import os
import json
import hashlib

RVGL_URL = "https://rvgl.re-volt.io/downloads/"

def get_checksum(filename):
    try:
        sha = hashlib.sha256()
        with open(filename, 'rb') as f:
            while chunk := f.read(256*1024):
                sha.update(chunk)
        return sha.hexdigest()
    except Exception as e:
        return ""

content = {}
with open(os.path.join("..", "rv_launcher", "version.py")) as f:
    exec(f.read(), content)

version = content["__version__"]

appinfo = {}
appinfo["version"] = version

for platform in ["win32", "win64", "linux"]:
    fname = f"rvgl_launcher_{platform}.zip"
    url = RVGL_URL + fname

    appinfo[platform] = {}
    appinfo[platform]["url"] = url
    appinfo[platform]["checksum"] = get_checksum(fname)

with open("rvgl_launcher.json", "w") as f:
    json.dump(appinfo, f, indent=4)
