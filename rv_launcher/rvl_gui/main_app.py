#!/usr/bin/env python3

import wx
import os
import sys
import threading
import traceback
import webbrowser
import queue

from rv_launcher.rvl_gui.tabs import *
from rv_launcher.rvl_gui.threads import *
from rv_launcher.rvl_gui.widgets import *
from rv_launcher.config import *
from rv_launcher.startup import *
from rv_launcher.helpers import *
from rv_launcher.logging import *
from rv_launcher.network import *
from rv_launcher.packages import Repo
from rv_launcher.events import EventCatalog
from rv_launcher.version import __version__
from rv_launcher import common
from rv_launcher.common import *


class FrameMain(wx.Frame):
    def __init__(self, *args, **kwargs):
        super(FrameMain, self).__init__(*args, **kwargs)
        self.lock = threading.Lock()
        self.url_queue = queue.Queue()
        self.worker = None
        self.update_worker = None
        self.timer = None

        self.init_ui()


    def init_ui(self):
        self.SetTitle(APP_TITLE)
        self.set_icon()

        self.SetMinSize(wx.Size(720, 480))
        self.SetSize(wx.Size(900, 600))
        self.Centre()

        self.Bind(wx.EVT_CLOSE, self.on_close)
        self.Bind(wx.EVT_PAINT, self.on_paint)
        self.Bind(wx.EVT_TIMER, self.on_timer)

        self.Bind(EVT_DOWNLOAD_PROGRESS, self.on_download_progress)
        self.Bind(EVT_UPDATE_APP, self.on_update_app_done)
        self.Bind(EVT_LAUNCH_GAME, self.on_launch_game_done)
        self.Bind(EVT_SHOW_MESSAGE, self.on_show_message)
        self.Bind(EVT_COLLECT_TASKS, self.on_collect_tasks_done)
        self.Bind(EVT_IPC_ARGS, self.on_ipc_args)

        # Main box
        self.box = wx.BoxSizer(wx.VERTICAL)

        # Tabs
        self.tab_bar = TabBar(self)
        self.game_tab = GameTab(self.tab_bar)
        self.events_tab = EventsTab(self.tab_bar)
        self.packs_tab = PacksTab(self.tab_bar)
        self.repository_tab = RepositoryTab(self.tab_bar)
        self.local_tab = LocalTab(self.tab_bar)
        self.console_tab = ConsoleTab(self.tab_bar)
        self.tab_bar.AddPage(self.game_tab, "Game")
        self.tab_bar.AddPage(self.events_tab, "Events")
        self.tab_bar.AddPage(self.packs_tab, "Packs")
        self.tab_bar.AddPage(self.repository_tab, "Repositories")
        self.tab_bar.AddPage(self.local_tab, "Local")
        self.tab_bar.AddPage(self.console_tab, "Console")
        self.console_tab.Hide()

        self.status_messages = ["Not doing anything"]
        self.launch_status = "Launching game..."
        self.update_status = ""

        self.status_box = wx.BoxSizer(wx.HORIZONTAL)
        self.status = wx.StaticText(self, -1, self.status_messages[-1])
        self.status_box.Add(self.status, 0, wx.EXPAND | wx.ALL, 2)
        self.status_box.AddSpacer(10)

        self.progress_box = wx.BoxSizer(wx.HORIZONTAL)
        self.progress_text = wx.StaticText(self, -1, "")
        self.progress_box.Add(self.progress_text, 0, wx.EXPAND | wx.ALL, 2)

        self.progress_bar = wx.Gauge(self, -1, style=wx.GA_HORIZONTAL | wx.GA_SMOOTH, size=(200,0))
        self.progress_box.Add(self.progress_bar, 0, wx.EXPAND | wx.ALL, 5)

        self.progress_percent = wx.StaticText(self, -1, "")
        self.progress_box.Add(self.progress_percent, 0, wx.EXPAND | wx.ALL, 2)
        self.status_box.Add(self.progress_box, 1, wx.EXPAND, 0)
        self.status_box.AddSpacer(10)
        self.hide_progress()

        self.status_box.AddStretchSpacer()
        self.version = wx.StaticText(self, -1, f"v{__version__}")
        self.status_box.Add(self.version, 0, wx.EXPAND | wx.ALL, 2)

        self.box.Add(self.tab_bar, 1, wx.EXPAND, 0)
        self.box.Add(self.status_box, 0, wx.EXPAND, 0)
        self.SetSizer(self.box)

        self.init_menubar()


    def init_menubar(self):
        self.menu_bar = wx.MenuBar()

        # File menu
        self.menu_files = wx.Menu()

        self.menu_files_open_data_dir = self.menu_files.Append(-1, "Open Data Folder", "Open data folder containing game files and downloads")
        self.Bind(wx.EVT_MENU, self.on_open_data_dir, self.menu_files_open_data_dir)

        self.menu_files_open_config_dir = self.menu_files.Append(-1, "Open Config Folder", "Open configuration folder containing settings files")
        self.Bind(wx.EVT_MENU, self.on_open_config_dir, self.menu_files_open_config_dir)

        self.menu_files.AppendSeparator()

        self.menu_files_clean = self.menu_files.Append(-1, "Clean", "Clean up game files")
        self.Bind(wx.EVT_MENU, self.on_clean, self.menu_files_clean)

        self.menu_files_repair = self.menu_files.Append(-1, "Repair", "Repair game files")
        self.Bind(wx.EVT_MENU, self.on_repair, self.menu_files_repair)

        self.menu_files_uninstall = self.menu_files.Append(-1, "Uninstall", "Uninstall game files")
        self.Bind(wx.EVT_MENU, self.on_uninstall, self.menu_files_uninstall)

        self.menu_files.AppendSeparator()

        self.menu_files_quit = self.menu_files.Append(wx.ID_EXIT, "Quit", "Quit application")
        self.Bind(wx.EVT_MENU, self.on_quit, self.menu_files_quit)

        self.menu_bar.Append(self.menu_files, "&File")

        # Events menu
        self.menu_events = wx.Menu()

        self.menu_events_update = self.menu_events.Append(-1, "Update Event Info", "Update online events info")
        self.Bind(wx.EVT_MENU, self.events_tab.on_update_event_info, self.menu_events_update)

        self.menu_bar.Append(self.menu_events, "&Events")

        # Packs menu
        self.menu_packs = wx.Menu()

        self.menu_packs_open_pack_dir = self.menu_packs.Append(-1, "Open Pack Folder", "Open pack folder containing installed content")
        self.Bind(wx.EVT_MENU, self.on_open_pack_dir, self.menu_packs_open_pack_dir)

        self.menu_packs.AppendSeparator()

        self.menu_packs_update = self.menu_packs.Append(-1, "Update Pack Info", "Update game packages info")
        self.Bind(wx.EVT_MENU, self.packs_tab.on_update_pack_info, self.menu_packs_update)

        self.menu_bar.Append(self.menu_packs, "&Packs")

        # Help menu
        self.menu_help = wx.Menu()
        self.menu_bar.Append(self.menu_help, "&Help")

        self.menu_help_docs = self.menu_help.Append(-1, "Open Documentation", "Open the documentation website")
        self.Bind(wx.EVT_MENU, self.on_open_documentation, self.menu_help_docs)

        self.menu_help.AppendSeparator()

        self.menu_help_console = self.menu_help.AppendCheckItem(-1, "Show Console", "Show console messages")
        self.Bind(wx.EVT_MENU, self.on_show_console, self.menu_help_console)
        self.menu_help_console.Check(CONFIG["show-console"])
        self.on_show_console(None)

        self.menu_help_uri = self.menu_help.Append(-1, "Register URI", "Let the launcher handle rvmm:// links")
        self.Bind(wx.EVT_MENU, self.on_register_uri, self.menu_help_uri)

        self.SetMenuBar(self.menu_bar)


    def set_icon(self):
        ext = ("png", "ico")[sys.platform == "win32"]
        icon_file = os.path.join("icons", f"icon.{ext}")
        if os.path.isfile(icon_file):
            self.SetIcon(wx.Icon(icon_file))


    def on_paint(self, e):
        # Put data into the GUI
        if not self.timer:
            self.init_packs()
            self.handle_arguments(sys.argv)

            self.timer = wx.Timer(self, -1)
            self.timer.Start(900 * 1000)
            self.on_timer(None)

        e.Skip()

    def on_close(self, e):
        if e.CanVeto() and self.get_worker():
            message = "\n".join(("Packages are being installed!",
                "Do you really want to cancel all pending tasks?"))
            if self.show_message("Warning", message) != wx.ID_YES:
                e.Veto()
                return

        close_event.set()

        if any(self.get_all_workers()):
            self.Disable()
            self.set_status("Cancelling all tasks...")

            worker = CollectTasksThread(self)
            worker.start()
            return

        e.Skip()

    def on_collect_tasks_done(self, e):
        self.set_worker(None)
        self.update_worker = None
        self.on_quit(None)

    def on_timer(self, e):
        self.refresh_packs()
        self.refresh_events()
        self.check_updates()

    def on_quit(self, e):
        self.Close()

    def on_show_message(self, e):
        title, message = e.value["title"], e.value["message"]
        if self.show_message(title, message) == wx.ID_YES:
            e.value["response"] = True

        message_event.set()

    def on_download_progress(self, e):
        if not e.value:
            self.hide_progress()
            return
        label, percent = e.value
        self.set_progress(label, percent)
        self.show_progress()

    def on_launch_game_done(self, e):
        self.clear_status(self.launch_status)
        self.Iconize(False)
        self.Enable()

    def on_update_app_done(self, e):
        if self.update_status:
            self.clear_status(self.update_status)
        self.update_status = e.value
        if self.update_status:
            self.set_status(self.update_status)

    def on_open_data_dir(self, e):
        open_folder(get_data_dir())

    def on_open_config_dir(self, e):
        open_folder(CONFIG_DIR)

    def on_open_pack_dir(self, e):
        self.packs_tab.open_pack_dir()

    def on_open_documentation(self, e):
        webbrowser.open("https://re-volt.gitlab.io/rvgl-launcher")

    def on_register_uri(self, e):
        register_uri()

    def on_show_console(self, e):
        CONFIG["show-console"] = self.menu_help_console.IsChecked()
        if sys.platform != "win32":
            self.console_tab.Show(CONFIG["show-console"])
            return

        # Page hiding is not supported on Windows...
        page = self.tab_bar.FindPage(self.console_tab)
        if CONFIG["show-console"] and page == -1:
            self.tab_bar.AddPage(self.console_tab, "Console")
        elif not CONFIG["show-console"] and page != -1:
            self.tab_bar.RemovePage(page)

    def on_clean(self, e):
        self.packs_tab.on_cleanup_game(e)

    def on_repair(self, e):
        message = "\n".join(("This will reinstall all packages.", "Continue?"))
        if self.show_message("Repair Game", message) == wx.ID_YES:
            self.packs_tab.on_update_game(e, clean=True)

    def on_uninstall(self, e):
        message = "\n".join(("This will remove all packages.", "Continue?"))
        if self.show_message("Uninstall Game", message) == wx.ID_YES:
            self.packs_tab.on_update_game(e, uninstall=True)

    def on_ipc_args(self, e):
        self.handle_arguments(e.value)
        self.Iconize(False)
        self.Raise()


    def handle_arguments(self, args):
        message = f"RVGL Launcher was run with arguments: {args}"
        # self.show_message("Information", message, style=wx.ICON_WARNING | wx.CENTRE)
        print_log(message)

        for arg in args:
            if arg.startswith(APP_URI):
                result = parse_uri(arg)
                if result["type"] == "download":
                    filename, url = result["filename"], result["url"]
                    print_log(f"Adding download to queue: {filename} => {url}")
                    self.url_queue.put((filename, url))
                elif result["type"] == "join":
                    ip = result["ip"]
                    self.launch_game(args=f"-lobby {ip}")

    def launch_game(self, recipe=None, args=""):
        recipe = recipe or common.recipes["default"]
        if self.packs_tab.IsEnabled() and not recipe.check():
            message = "\n".join(("Packages need to be installed.", "Install them now?"))
            if self.show_message("Update Game", message) == wx.ID_YES:
                self.packs_tab.on_update_game(None, recipes=[recipe])
                return

        self.Disable()
        self.Iconize(True)
        self.set_status(self.launch_status)

        worker = LaunchGameThread(self, recipe, args)
        worker.start()


    def show_message(self, title, message, style=wx.YES_NO | wx.ICON_WARNING | wx.CENTRE):
        with wx.MessageDialog(self, message, title, style) as dialog:
            return dialog.ShowModal()


    def get_all_workers(self):
        return [self.get_worker(), self.update_worker]

    def get_worker(self):
        with self.lock:
            worker = self.worker
        return worker

    def set_worker(self, worker):
        with self.lock:
            self.worker = worker


    def set_status(self, status):
        self.status_messages.append(status)
        self.status.SetLabel(status)
        self.status_box.Layout()

    def clear_status(self, status):
        if status in self.status_messages:
            self.status_messages.remove(status)
        self.status.SetLabel(self.status_messages[-1])
        self.status_box.Layout()

    def show_progress(self):
        self.status_box.Show(self.progress_box)
        self.status_box.Layout()

    def hide_progress(self):
        self.status_box.Hide(self.progress_box)
        self.status_box.Layout()

    def set_progress(self, label, percent):
        self.progress_text.SetLabel(label)
        self.progress_percent.SetLabel(f"{percent}%")
        self.progress_bar.SetValue(percent)
        self.status_box.Layout()


    def check_updates(self):
        if self.update_worker:
            return

        self.update_status = ""

        worker = UpdateAppThread(self)
        self.update_worker = worker
        worker.start()


    def init_packs(self):
        self.refresh_packs(fetch=False)

    def refresh_packs(self, fetch=True, local=False):
        self.packs_tab.on_update_pack_info(None, fetch=fetch, local=local)

    def refresh_events(self):
        self.events_tab.on_update_event_info(None)


    def disable_controls(self):
        self.menu_files_clean.Enable(False)
        self.menu_files_repair.Enable(False)
        self.menu_files_uninstall.Enable(False)

    def enable_controls(self):
        enable = game_installed()
        self.menu_files_clean.Enable(True)
        self.menu_files_repair.Enable(enable)
        self.menu_files_uninstall.Enable(enable)

    def disable_game(self):
        self.game_tab.Disable()
        self.events_tab.event_list.Disable()
        self.events_tab.hide_controls()

    def enable_game(self):
        self.game_tab.Enable()
        self.events_tab.event_list.Enable()
        self.events_tab.show_controls()

    def disable_packs(self):
        if not game_installed():
            self.game_tab.Disable()
        self.disable_controls()
        self.game_tab.choice_preset.Disable()
        self.menu_packs_update.Enable(False)
        self.packs_tab.Disable()
        self.repository_tab.set_state()
        self.local_tab.Disable()

    def enable_packs(self):
        if not game_installed():
            self.game_tab.Enable()
        self.enable_controls()
        self.game_tab.choice_preset.Enable()
        self.menu_packs_update.Enable(True)
        self.packs_tab.Enable()
        self.repository_tab.set_state()
        self.local_tab.Enable()

    def disable_events(self):
        self.menu_events_update.Enable(False)
        self.events_tab.Disable()
        self.repository_tab.set_state()

    def enable_events(self):
        self.menu_events_update.Enable(True)
        self.events_tab.Enable()
        self.repository_tab.set_state()


""" Catches exceptions in wx event handlers that try blocks cannot """
def excepthook(etype, value, trace):
    message = "".join(traceback.format_exception(etype, value, trace))
    print_log(message)

    title = "Unhandled Exception"
    style = wx.ICON_ERROR | wx.CENTRE
    with wx.MessageDialog(None, message, title, style) as dialog:
        dialog.ShowModal()

    exit()


def safe_main():
    app = wx.App()

    name = f"{APP_NAME}-{APP_ID}-{wx.GetUserId()}"
    instance = wx.SingleInstanceChecker(name, path=DATA_DIR)
    if instance.IsAnotherRunning():
        # style = wx.ICON_ERROR | wx.CENTRE
        # message = f"{APP_TITLE} is already running!"
        # with wx.MessageDialog(None, message, "Error", style) as dialog:
        #     dialog.ShowModal()
        ipc = IpcHelper(name)
        return ipc.client()

    common.log_file = LogFile()
    common.repo = Repo()
    common.event_catalog = EventCatalog()

    prepare_config()
    prepare_folders()
    prepare_recipes()

    app.SetAppName(APP_NAME)
    app.SetAppDisplayName(APP_TITLE)
    app.SetClassName(APP_TITLE)

    frame = FrameMain(None)
    frame.Show()

    worker = IpcListenerThread(name, frame)
    worker.start()

    app.SetTopWindow(frame)
    app.MainLoop()

    save_config()


def main():
    sys.excepthook = excepthook
    safe_main()


if __name__ == '__main__':
    main()

