import os
import sys
import socket
from multiprocessing.connection import Client, Listener
from rv_launcher import common
from rv_launcher.common import *
from rv_launcher.helpers import *


""" Wrapper class for IPC client and listener """
class IpcHelper():
    def __init__(self, name):
        self.name = name

    """ Gets an address based on the best available protocol """
    def get_address(self):
        if hasattr(socket, "AF_UNIX"):
            return os.path.join(DATA_DIR, f"{self.name}.socket")
        elif sys.platform == "win32":
            return rf"\\.\pipe\{self.name}"
        else:
            return ("localhost", 6310)

    """ Connects to the server and sends launch arguments """
    def client(self):
        try:
            with Client(self.get_address(), authkey=self.name.encode()) as conn:
                conn.send({"args": sys.argv})
        except Exception as e:
            print_log(f"IPC client failed to connect.\n  {e}")

    """ Listens for connections and processes messages """
    def server(self):
        try:
            os.remove(os.path.join(DATA_DIR, "socket.tmp"))
        except Exception as e:
            pass
        try:
            with Listener(self.get_address(), authkey=self.name.encode()) as listener:
                yield from self.process(listener)
        except Exception as e:
            print_log(f"IPC server failed to connect.\n  {e}")

    def process(self, listener):
        while not close_event.is_set():
            try:
                with listener.accept() as conn:
                    msg = conn.recv()
                    if self.validate(msg):
                        yield msg
            except Exception as e:
                print_log(f"IPC server failed to receive.\n  {e}")
                continue

    def validate(self, msg):
        if not isinstance(msg, dict):
            return False

        for key, value in msg.items():
            # key must be a string
            if not isinstance(key, str):
                return False
            # value must be a list
            if not isinstance(value, list):
                return False
            # of strings
            if not all([isinstance(i, str) for i in value]):
                return False

        return True

