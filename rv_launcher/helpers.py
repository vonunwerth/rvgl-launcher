import os
import sys
import re
import glob
import subprocess
import hashlib
from pathlib import Path
from urllib.parse import urlparse, parse_qs, urlunparse
from datetime import datetime, timedelta, timezone
from shutil import copytree, rmtree
from rv_launcher import common
from rv_launcher.common import *
from rv_launcher.logging import *

if sys.platform == "win32":
    import winreg


""" Runs a command in shell and returns the output """
def run_command(command):
    print_log(f"Running command ({command}).")
    out, err = subprocess.Popen(command, stdout=subprocess.PIPE,
        stdin=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True).communicate()
    return out.decode()


""" Creates a folder if it does not exist """
def create_folder(folder):
    if not os.path.isdir(folder):
        try:
            os.makedirs(folder)
        except Exception as e:
            print_log(f"Could not create folder '{folder}'.\n  {e}")


""" Clears out the contents of a folder """
def clean_folder(folder):
    if not os.path.isdir(folder):
        return

    with os.scandir(folder) as entries:
        for entry in entries:
            try:
                if entry.is_file() or entry.is_symlink():
                    os.remove(entry.path)
                elif entry.is_dir():
                    rmtree(entry.path)
            except Exception as e:
                print_log(f"Could not remove folder entry '{entry.path}'.\n  {e}")


""" Removes a folder and all of its contents """
def remove_folder(folder):
    if os.path.isdir(folder):
        try:
            rmtree(folder)
        except Exception as e:
            print_log(f"Could not remove folder '{folder}'.\n  {e}")


""" Moves the contents of source folder and merges them with dest """
def merge_folders(src, dest, fix_cases=False):
    try:
        files = glob.glob(os.path.join(src, "**"), recursive=True)
        files.sort()
    except Exception as e:
        print_log(f"Could not merge folders '{src}' => '{dest}'.\n  {e}")
        return False

    success = True

    for entry in files:
        try:
            out = os.path.relpath(entry, src)
            out = out.lower() if fix_cases else out
            out = os.path.join(dest, out)
            if os.path.isdir(entry):
                create_folder(out)
            else:
                os.replace(entry, out)
        except Exception as e:
            print_log(f"Could not move folder entry '{entry}'.\n  {e}")
            success = False

    return success


""" Copies a folder with optional symlink support """
def copy_folder(src_dir, dst_dir, symlinks=False):
    try:
        if symlinks and sys.platform == "linux":
            create_folder(dst_dir)
            src_dir = os.path.abspath(src_dir)
            command = f'cp -asf "{abs_src_dir}"/* "{dst_dir}"'
            run_command(command)
        else:
            # dirs_exist_ok requires Python 3.8+
            copytree(src_dir, dst_dir, dirs_exist_ok=True)
    except Exception as e:
        print_log(f"Could not copy folder ('{src_dir}' -> '{dst_dir}').\n  {e}")


""" Resolve game data and config directories """
def get_data_dir():
    if CONFIG["install-type"] == "standard":
        return DATA_DIR
    else:
        return CONFIG["data-dir"]

def get_save_dir():
    if CONFIG["install-type"] == "standard":
        return os.path.join(DATA_DIR, "save")
    else:
        return CONFIG["save-dir"]

def get_launch_dir():
    if CONFIG["install-type"] == "standard":
        return common.repo.get_package_dir(f"rvgl_{PLATFORM}")
    else:
        return CONFIG["launch-dir"]

def game_installed():
    return CONFIG["installed"]


def find_game(folder):
    if not os.path.isdir(folder):
        return False

    for dname in ["cars", "gfx", "levels"]:
        if not os.path.isdir(os.path.join(folder, dname)):
            return False

    for fname in ["rvgl.exe", "rvgl"]:
        if os.path.isfile(os.path.join(folder, fname)):
            return True

    return False


def find_save(folder):
    if not os.path.isdir(folder):
        return False

    for dname in ["profiles", "replays", "times"]:
        if os.path.isdir(os.path.join(folder, dname)):
            return True

    return False


def launch_game(recipe="default", args=""):
    print_log("Launching game...")
    command = os.path.join(get_launch_dir(), "rvgl")
    options = " ".join([
        f'-basepath "{get_data_dir()}"',
        f'-prefpath "{get_save_dir()}"',
        f'-packlist "{recipe}"',
        f'{CONFIG["launch-params"]}',
        f'{args}'])

    try:
        run_command(f'"{command}" {options}')
    except Exception as e:
        print_log(f"Could not launch game.\n  {e}")

    print_log("Game closed.")


def open_folder(path):
    if sys.platform == "darwin":
        subprocess.Popen(["open", path])
    elif sys.platform == "linux":
        subprocess.Popen(["xdg-open", path])
    elif sys.platform == "win32":
        subprocess.Popen(["explorer", path])


""" Extracts any supported archive format using 7-Zip """
def extract_archive(path, dest):
    command = ("7z", "7z.exe")[sys.platform == "win32"]
    command = os.path.join(get_dist_path(), command)
    options = f'x "{path}" -y -o"{dest}"'

    try:
        output = run_command(f'"{command}" {options}')
        return "Everything is Ok" in output
    except Exception as e:
        print_log(f"Could not launch 7-Zip.\n  {e}")
        return False


""" Checks whether an archive has correct folder structure for content """
def validate_archive(path):
    command = ("7z", "7z.exe")[sys.platform == "win32"]
    command = os.path.join(get_dist_path(), command)
    options = f'l "{path}"'

    try:
        output = run_command(f'"{command}" {options}')
    except Exception as e:
        print_log(f"Could not launch 7-Zip.\n  {e}")
        return True # don't bother validating if we can't launch 7-zip

    try:
        files, offset = [], 0
        for i in output.splitlines():
            section = i.startswith("-----")
            if not offset and section:
                offset = i.rfind(" ") + 1
            elif section:
                break
            elif offset:
                files.append(i[offset:])

        types = common.repo.get_content_types()
        return all(f.startswith(types) for f in files)
    except Exception as e:
        print_log(f"Could not parse 7-Zip output.\n  {e}")
        return True # don't bother validating if the output cannot be parsed


""" Downloads a file to the specified destination file.
    Use force to re-download existing files. """
def download_file(url, dest, force=False, resume=True):
    if close_event.is_set():
        yield -1
    if not force and os.path.isfile(dest):
        print_log(f"File {dest} exists.")
        return

    try:
        r = session.head(url, timeout=15, allow_redirects=True)
        r.raise_for_status()
        size = int(r.headers.get("content-length", 0))
    except Exception as e:
        print_log(f"Could not fetch headers for {url}.\n  {e}")
        yield -1

    try:
        print_log(f"Downloading: {url} ({pretty_size(size)})")
        dlfile = dest + ".part"
        part_size = 0
        headers = None
        mode = "wb"

        if resume and os.path.isfile(dlfile):
            part_size = Path(dlfile).stat().st_size
            print_log(f"Resuming from {pretty_size(part_size)}...")
            headers = {"Range": f"bytes={part_size}-"}
            mode = "ab"

        percent = lambda: (part_size * 100 // size) if size else 0
        with session.get(url, timeout=15, stream=True, headers=headers) as r:
            r.raise_for_status()
            with open(dlfile, mode) as f:
                yield percent()
                for chunk in r.iter_content(chunk_size=256*1024):
                    f.write(chunk) # FIXME: Handle encoded content?
                    part_size += len(chunk)
                    yield percent()
                    if close_event.is_set():
                        yield -1

        if os.path.isfile(dest):
            os.remove(dest)
        os.rename(dlfile, dest)
    except Exception as e:
        print_log(f"Could not download file {url}.\n  {e}")
        yield -1


""" Verifies the SHA-256 hash of the supplied file """
def verify_file(filename, checksum):
    if not checksum:
        return True
    try:
        sha = hashlib.sha256()
        print_log(f"Verifying {filename}")
        with open(filename, 'rb') as f:
            while chunk := f.read(256*1024):
                sha.update(chunk)
        return sha.hexdigest() == checksum
    except Exception as e:
        print_log(f"Could not verify checksum for {filename}.\n  {e}")
        return False


""" Gets size in bytes and returns a pretty string """
def pretty_size(size, suffix='B'):
    for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
        if abs(size) < 1024.0:
            return f"{size:3.1f}{unit}{suffix}"
        size /= 1024.0
    return f"{size:.1f}Yi{suffix}"


""" Returns the MD5 hash of a unicode string """
def hashed_string(text):
    return hashlib.md5(text.encode()).hexdigest()


""" Convert to lowercase and remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert spaces to underscores. Also strip
    leading and trailing whitespace, dashes, and underscores. """
def slugify(name):
    name = re.sub(r"[^\w\s-]", "", name.lower())
    return re.sub(r"[_\s]+", "_", name).strip("-_")


""" Parses a time and date string into datetime structure """
def time_parse(time):
    # Workaround for stupid locale issue with wxPython 4.1 on Windows.
    # https://github.com/wxWidgets/Phoenix/issues/1637
    # if sys.platform == "win32":
    #     a = time.split(" ")
    #     a = a[0].split("-") + a[1].split(":")
    #     a = list(map(int, a))
    #     return datetime(day=a[0], month=a[1], year=a[2], hour=a[3], minute=a[4])

    # Just keep wxPython frozen to 4.0.7 on Windows
    return datetime.strptime(time, "%d-%m-%Y %H:%M")


""" Returns difference in seconds between the supplied
    UTC time and current local time """
def time_diff(time):
    try:
        dt = time_parse(time)
        dt = dt.replace(tzinfo=timezone.utc)
        diff = dt.astimezone() - datetime.now().astimezone()
        return int(diff.total_seconds())
    except Exception as e:
        print_log(f"Could not get time difference.\n  {e}")
        return int(timedelta(days=1).total_seconds())


""" Gets time difference in seconds and returns a pretty string """
def pretty_time(diff):
    diff = timedelta(seconds=abs(diff))

    second_diff = int(diff.seconds)
    day_diff = int(diff.days)

    if day_diff == 0:
        if second_diff == 1:
            return "a second"
        if second_diff < 60:
            return str(second_diff) + " seconds"
        if second_diff < 120:
            return "a minute"
        if second_diff < 3600:
            return str(second_diff // 60) + " minutes"
        if second_diff < 7200:
            return "an hour"
        if second_diff < 86400:
            return str(second_diff // 3600) + " hours"

    if day_diff == 1:
        return "a day"
    if day_diff < 7:
        return str(day_diff) + " days"
    if day_diff < 14:
        return "a week"
    if day_diff < 30:
        return str(day_diff // 7) + " weeks"
    if day_diff < 60:
        return "a month"
    if day_diff < 365:
        return str(day_diff // 30) + " months"
    if day_diff < 730:
        return "an year"

    return str(day_diff // 365) + " years"


""" Creates a Desktop shortcut on Windows or an App Menu
    shortcut (*.desktop file) on other platforms """
def create_shortcut():
    try:
        print_log("Creating shortcut...")
        command, args = get_app_command()
        path = get_app_path()

        if sys.platform == "win32":
            icon = os.path.join(path, "icons", "icon.ico")
            helper = os.path.join(get_dist_path(), "helper.exe")
            run_command(
                f'{helper} -path "{path}" '
                f'-command "{command}" -args "{args}" '
                f'-icon "{icon}" -shortcut'
            )
        else:
            print_log(f"Creating shortcut file for '{path}'.")
            icon = os.path.join(path, "icons", "icon.png")
            content = (
                f'[Desktop Entry]\n'
                f'Comment=Launcher and package manager for RVGL\n'
                f'Terminal=false\n'
                f'Name=RVGL Launcher\n'
                f'Type=Application\n'
                f'Categories=Game;\n'
                f'Path={path}\n'
                f'Exec="{command}" "{args}"\n'
                f'Icon={icon}\n'
            )
            path = os.getenv("XDG_DATA_HOME", os.path.expanduser("~/.local/share"))
            path = os.path.join(path, "applications", "RVGL Launcher.desktop")
            with open(path, "w") as f:
                f.write(content)
    except Exception as e:
        print_log(f"Could not create shortcut file.\n  {e}")


""" Registers the custom URI for the launcher """
def register_uri():
    try:
        print_log("Registering URI...")
        command, args = get_app_command()
        path = get_app_path()

        if sys.platform == "win32":
            icon = os.path.join(path, "icons", "icon.ico")
            with winreg.CreateKeyEx(winreg.HKEY_CURRENT_USER, r"Software\Classes\rvmm") as hkey:
                winreg.SetValueEx(hkey, "", 0, winreg.REG_SZ, "URL:RVGL Launcher protocol")
                winreg.SetValueEx(hkey, "URL Protocol", 0, winreg.REG_SZ, "")
                winreg.SetValue(hkey, "DefaultIcon", winreg.REG_SZ, f'{icon}')
                winreg.SetValue(hkey, r"shell\open\command", winreg.REG_SZ, f'"{command}" "{args}" "%1"')
        else:
            print_log(f"Registering URI for '{path}'.")
            icon = os.path.join(path, "icons", "icon.png")
            content = (
                f'[Desktop Entry]\n'
                f'Name=RVGL Launcher\n'
                f'Type=Application\n'
                f'Categories=Game;\n'
                f'Path={path}\n'
                f'Exec="{command}" "{args}" %u\n'
                f'Icon={icon}\n'
                f'Terminal=false\n'
                f'StartupNotify=false\n'
                f'NoDisplay=true\n'
                f'MimeType=x-scheme-handler/rvmm;\n'
            )
            path = os.getenv("XDG_DATA_HOME", os.path.expanduser("~/.local/share"))
            path = os.path.join(path, "applications", "rvmm-scheme.desktop")
            with open(path, "w") as f:
                f.write(content)

            run_command("xdg-mime default rvmm-scheme.desktop x-scheme-handler/rvmm")

        CONFIG["uri-registered"] = True
    except Exception as e:
        print_log(f"Could not create shortcut file.\n  {e}")


""" Parses the passed URI string into a filename and URL """
def parse_uri(arg):
    result = {"type": "none"}

    try:
        u = urlparse(arg)
        domain = u.netloc
        if domain.startswith("www."):
            domain = domain[4:]

        if domain == "join":
            ip = os.path.basename(os.path.normpath(u.path))
            result["type"] = "join"
            result["ip"] = ip
            return result

        if domain == "revoltworld.net":
            q = parse_qs(u.query)
            filename = q["filename"][0]
            url = urlunparse(u._replace(scheme="https"))
        elif domain in ("revoltzone.net", "revoltxtg.co.uk"):
            filename = os.path.basename(u.path)
            url = urlunparse(u._replace(scheme="http"))
        elif domain in ("files.re-volt.io", "hajduc.com"):
            filename = os.path.basename(u.path)
            url = urlunparse(u._replace(scheme="https"))
        else:
            print_log(f"Downloads from '{u.netloc}' are not supported.")
            return result

        result["type"] = "download"
        result["filename"] = filename
        result["url"] = url
    except Exception as e:
        print_log(f"Could not parse URI '{arg}'.\n  {e}")

    return result

