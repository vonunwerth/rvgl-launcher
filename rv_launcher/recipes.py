import os
import json
from rv_launcher import common
from rv_launcher.common import *
from rv_launcher.backup import *
from rv_launcher.helpers import *


class Recipe:

    def __init__(self, packages, title, description="", slug="", install=False, local=False, writable=True, root=False):
        self.title = title
        self.description = description
        self.slug = slug if slug else slugify(self.title)
        self.packages = packages  # List of packages required for the game installation
        self.writable = writable  # Whether the user editable pack is enabled
        self.root = root  # Whether root content is enabled
        self.install = install  # Whether the recipe is installable
        self.local = local  # Whether the recipe is locally selectable


    """ Add list of packages to the recipe """
    def add_packages(self, plist):
        for package in plist:
            if package == "local":
                self.writable = True
                continue
            if package == "default":
                self.root = True
                continue

            if not common.repo.package_exists(package):
                continue
            if common.repo.is_platform_package(package):
                continue
            if self.has_package(package):
                continue

            self.packages.append(package)

        self.sort_packages()


    """ Remove list of packages from the recipe """
    def remove_packages(self, plist):
        for package in plist:
            if package == "local":
                self.writable = False
                continue
            if package == "default":
                self.root = False
                continue

            if not common.repo.package_exists(package):
                continue
            if common.repo.is_platform_package(package):
                continue
            if not self.has_package(package):
                continue

            self.packages.remove(package)


    """ Check whether the package is part of the recipe """
    def has_package(self, package):
        if package == f"rvgl_{PLATFORM}":
            return True
        if package == "local" and self.writable:
            return True
        if package == "default" and self.root:
            return True

        return package in self.packages


    """ Sort packages based on the master package list """
    def sort_packages(self):
        plist = []
        for pack in common.repo.package_info:
            if pack in self.packages:
                plist.append(pack)

        self.packages = plist


    """ Returns the list of packages in this recipe """
    def get_packages(self):
        plist = []
        for pack in common.repo.package_info:
            if self.has_package(pack):
                plist.append(pack)

        return plist


    """ Makes sure all packages required for the recipe are present and updated """
    def check(self):
        for package in self.packages + [f"rvgl_{PLATFORM}"]:
            if not common.repo.package_exists(package):
                return False

            installed = common.repo.is_installed(package)
            outdated = common.repo.is_outdated(package)
            if not installed or outdated:
                return False

        return True


    """ Removes all package files from the game installation """
    def clean(self):
        for package in self.packages + [f"rvgl_{PLATFORM}"]:
            if common.repo.package_exists(package):
                common.repo.remove_package(package)
            else:
                print_log(f"Could not remove package ({package} is unknown)")


    """ Downloads and installs all packages required for the recipe """
    def prepare(self):
        if self.check():
            yield None
            return

        for package in self.packages + [f"rvgl_{PLATFORM}"]:
            if common.repo.package_exists(package):
                yield from common.repo.install_package(package)
            else:
                print_log(f"Could not add package ({package} is unknown)")

        yield None


    """ Creates a game installation with the specified packages """
    def cook(self):
        # dest_dir = os.path.join(DATA_DIR, "game")
        # print_log("Backing up profile...")
        # backup_profile()
        # print_log("Cleaning folder...")
        # clean_folder(dest_dir)
        #
        # for package in self.packages:
        #     print_log(f"Installing {package}...")
        #     source_dir = os.path.join(DATA_DIR, "packs", package)
        #     if os.path.isdir(source_dir):
        #         copy_folder(source_dir, dest_dir, symlinks=True)
        #
        # print_log("Applying backup...")
        # apply_profile()

        if not self.local:
            return

        if self.writable:
            local_dir = os.path.join(get_data_dir(), "packs", "local")
            create_folder(local_dir)

        self.export_packlist()


    def load(self):
        fname = f"{self.slug}.json"
        print_log(f"Loading recipe: {fname}")
        recipefile = os.path.join(CONFIG_DIR, "recipes", fname)
        try:
            with open(recipefile, "r") as f:
                as_json = json.load(f)
        except Exception as e:
            print_log(f"Could not load recipe file {fname}.\n  {e}")
            return

        self.title = as_json.get("title", self.slug)
        self.description = as_json.get("description", "")
        self.packages = as_json.get("packages", [])
        self.writable = as_json.get("writable", False)
        self.root = as_json.get("root", False)


    def save(self):
        as_json =  {
            "title": self.title,
            "description": self.description,
            "packages": self.packages,
            "writable": self.writable,
            "root": self.root
        }

        fname = f"{self.slug}.json"
        print_log(f"Saving recipe: {fname}")
        recipefile = os.path.join(CONFIG_DIR, "recipes", fname)
        try:
            with open(recipefile, "w") as f:
                json.dump(as_json, f, indent=4)
        except Exception as e:
            print_log(f"Could not save recipe file {fname}.\n  {e}")


    def remove(self):
        fname = f"{self.slug}.json"
        print_log(f"Removing recipe: {fname}")
        recipefile = os.path.join(CONFIG_DIR, "recipes", fname)
        try:
            os.remove(recipefile)
        except Exception as e:
            print_log(f"Could not remove recipe file {fname}.\n  {e}")

        self.remove_packlist()


    def export_packlist(self):
        fname = f"{self.slug}.txt"
        print_log(f"Exporting packlist: {fname}")
        packfile = os.path.join(get_data_dir(), "packs", fname)
        try:
            with open(packfile, "w") as f:
                if self.root:
                    f.write(f'"default"\n')
                for package in self.packages:
                    f.write(f'"{package}"\n')
                if self.writable:
                    f.write(f'"local" *\n')
        except Exception as e:
            print_log(f"Could not export packlist {fname}.\n  {e}")


    def remove_packlist(self):
        fname = f"{self.slug}.txt"
        print_log(f"Removing packlist: {fname}")
        packfile = os.path.join(get_data_dir(), "packs", fname)
        try:
            os.remove(packfile)
        except Exception as e:
            print_log(f"Could not remove packlist {fname}.\n  {e}")

