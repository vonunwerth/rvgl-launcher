# Local Content

<!-- toc -->

There is a special pack called `local` which is meant to hold content that do
not belong to any other pack. This can be content you downloaded from a website
like [Re-Volt World](https://www.revoltworld.net) or [Re-Volt Zone](http://revoltzone.net).
There is a dedicated `Local` tab that lets you manage content in this `local` pack.

[![Packs tab](screens/local.th.png)](screens/local.png)

## Features

You can use the `Local` tab to:

- Install content from archives (`zip`, `7z` or `rar`).
- View installed content by type (cars, levels, cups, ...).
- Selectively remove previously installed content.

## Adding Content

Drag and drop any supported archive file into the text view at the bottom. This
will cause the content to be installed to your local pack. The text view shows
you the installation results, including any errors or warnings.

The content is validated before installing to ensure that the correct folder
structure is used. Also, filenames are automatically converted to lower case for
cross-platform compatibility.

## Managing Content

Once you've added some content, you can view files in your `local` pack
filtered by the type of content. This can be cars, levels, cups and so on.

- Select the content type from the drop-down list to view all content of that
  type. This can be folders (in case of cars and levels) or files (in case of
  cups and gfx).
- Double click on any of the listed content to open that folder.
- Remove content by selecting it from the list and clicking on `Remove Selected`.
  Multiple selections are possible.

